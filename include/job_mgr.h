/**
 * @file job_mgr.h
 */

#ifndef JOB_MGR_H
#define JOB_MGR_H

/* Each UNIX domain socket pair requires two sockets,
   one for each end.  The below constants are used to
   manage which pair is used for what purpose.
*/
#define JOBMGR_OTHER_SOCK 0
#define JOBMGR_MGR_SOCK   1

typedef struct listener_fd
{
    int fds[2];
} listener_fd_t;

typedef struct worker_fd
{
    int fds[2];
} worker_fd_t;

typedef struct job_mgr_ctx
{
    listener_fd_t *listener;
    worker_fd_t *worker;  /* Not known until run-time */
    int ctrl_read_end_fd;
    int num_wrkrs;
    int num_conns;        /* 2 + num_wrkrs */
} job_mgr_ctx_t;

/**
 * @brief Allocates memory, fills in state, and establishes
 *        UNIX domain socket pairs for job manager.
 * 
 * Takes in the total number of worker threads which will service
 * job requests and returns a pointer to a job_mgr_ctx_t.  The 
 * job manager communicates with a listening server, control pipe, 
 * and num_workers number of worker threads.  To establish this
 * service, UNIX domain socket pairs are created for the listening
 * server and each worker thread.  The control pipe utilizes pipes
 * which have already been established ahead of the call to this
 * function.
 * 
 * The returned pointer to the job_mgr_ctx_t structure contains
 * valid file descriptors which will be used by the job manager
 * in the job_mgr_run function.
 * 
 * @param num_workers Integer number of worker threads supported
 *        by job manager.
 * @param ctrl_read_fd A file descriptor for the read end of the
 *        control pipe used by the main thread to communicate
 *        with each service in the program.
 * @return A pointer to a job_mgr_ctx_t structure on success or
 *         NULL on failure.
 * @note Does not check either parameter.
 * @note The caller must manage memory for the returned job_mgr_ctx_t
 *       structure.
 */
job_mgr_ctx_t *job_mgr_init(int num_workers, int ctrl_read_fd);


/**
 * @brief 
 */
void job_mgr_destroy(job_mgr_ctx_t **mgr);

/**
 * TODO: run prototype
 * @brief 
 */
#endif /* JOB_MGR_H */