/**
 * @file job_mgr.c
 * 
 * Provides the functions required to run a job manager
 * in its own thread.  This unit provides initialization,
 * destruction, and running.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include "job_mgr.h"

// TODO: Add two linked lists; (1) job_requests, (2) job_results
/**
 * @brief 
 */
static job_mgr_ctx_t *job_mgr_allocate_ctx(int num_workers)
{
    job_mgr_ctx_t *jm_ctx;

    jm_ctx = (job_mgr_ctx_t *) malloc(sizeof(job_mgr_ctx_t));
    if (NULL == jm_ctx)
    {
        fprintf(stderr, "Error: malloc failed in job_mgr_init.\n");
        return NULL;
    }

    jm_ctx->listener = (listener_fd_t *) malloc(sizeof(listener_fd_t));
    if (NULL == jm_ctx)
    {
        fprintf(stderr, "Error: malloc failed in job_mgr_init.\n");
        free(jm_ctx);
        return NULL;
    }

    /* Each thread requires 2 fds for socketpair */
    jm_ctx->worker = (worker_fd_t *) malloc(num_workers * sizeof(worker_fd_t));
    if (NULL == jm_ctx->worker)
    {
        fprintf(stderr, "Error: malloc failed in job_mgr_init.\n");
        free(jm_ctx->listener);
        free(jm_ctx);
        return NULL;
    }

    /* Set to -1 to use later in determining which ones were 
       successfully created with call to socketpair.
    */
    jm_ctx->listener->fds[JOBMGR_OTHER_SOCK] = -1;
    jm_ctx->listener->fds[JOBMGR_MGR_SOCK] = -1;

    for (int i = 0; i < num_workers; i++)
    {
        jm_ctx->worker[i].fds[JOBMGR_OTHER_SOCK] = -1;
        jm_ctx->worker[i].fds[JOBMGR_MGR_SOCK] = -1;
    }

    return jm_ctx;
}

/**
 * @brief 
 */
static bool job_mgr_get_sockets(job_mgr_ctx_t *mgr)
{
    int rv;

    rv = socketpair(AF_UNIX, SOCK_SEQPACKET, 0, mgr->listener->fds);
    if (rv < 0)
    {
        perror("socketpair failed");
        job_mgr_destroy(&mgr);
        return false;
    }
    
    for (int i = 0; i < mgr->num_wrkrs; i++)
    {
        rv = socketpair(AF_UNIX, SOCK_SEQPACKET, 0, mgr->worker[i].fds);
        if (rv < 0)
        {
            perror("socketpair failed");
            job_mgr_destroy(&mgr);
            return false;
        }
    }

    return true;
}

job_mgr_ctx_t *job_mgr_init(int num_workers, int ctrl_read_fd)
{
    job_mgr_ctx_t *jm_ctx;
    bool sockets_created;

    jm_ctx = job_mgr_allocate_ctx(num_workers);
    if (NULL == jm_ctx)
    {
        fprintf(stderr, "Error: could not allocate memory for job manager.\n");
        return NULL;
    }

    jm_ctx->ctrl_read_end_fd = ctrl_read_fd;
    jm_ctx->num_wrkrs = num_workers;
    jm_ctx->num_conns = 2 + num_workers;

    sockets_created = job_mgr_get_sockets(jm_ctx);
    if (!sockets_created)
    {
        fprintf(stderr, "Error: could not create sockets for job manager.\n");
        return NULL;
    }

    return jm_ctx;
}

// TODO: destroy job_request and job_result linked lists
void job_mgr_destroy(job_mgr_ctx_t **mgr)
{
    if (NULL == *mgr)
    {
        return;
    }

    if (-1 != (*mgr)->listener->fds[JOBMGR_OTHER_SOCK] ||
        -1 != (*mgr)->listener->fds[JOBMGR_MGR_SOCK])
    {
        close((*mgr)->listener->fds[JOBMGR_OTHER_SOCK]);
        close((*mgr)->listener->fds[JOBMGR_MGR_SOCK]);
    }

    for (int i = 0; i < (*mgr)->num_wrkrs; i++)
    {
        if (-1 != (*mgr)->worker[i].fds[JOBMGR_MGR_SOCK] ||
            -1 != (*mgr)->worker[i].fds[JOBMGR_OTHER_SOCK])
        {
            close((*mgr)->worker[i].fds[JOBMGR_OTHER_SOCK]);
            close((*mgr)->worker[i].fds[JOBMGR_MGR_SOCK]);
        }
    }

    free((*mgr)->listener);
    free((*mgr)->worker);
    free(*mgr);
    *mgr = NULL;/* Prevent use after free */
}

// TODO: run

/* Used for testing */
int main(void)
{
    job_mgr_ctx_t *mgr;

    mgr = job_mgr_init(10, 4);
    job_mgr_destroy(&mgr);

    return 0;
}